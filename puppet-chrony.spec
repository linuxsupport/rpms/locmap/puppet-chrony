Name:           puppet-chrony
Version:        1.4
Release:        1%{?dist}
Summary:        Masterless puppet module for chrony

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Puppet Chrony  module for locmap

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/chrony/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/chrony/
touch %{buildroot}/%{_datadir}/puppet/modules/chrony/linuxsupport

%files -n puppet-chrony
%{_datadir}/puppet/modules/chrony
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Mon Oct 14 2024 CERN Linux Droid <linux.ci@cern.ch> - 1.4-0
- Rebased to #ec210d13 by locmap-updater

* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 1.3-1
-Add autoreconfigure %post script

* Fri Sep 29 2023 Manuel Guijarro <manuel.guijarro@cern.ch> 1.2-2
- Updated CERN subnets based on what is listed at: https://landb.cern.ch/portal/ip-networks

* Tue Jul 11 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> 1.2-1
- Use pool.ntp.org if the host is not on the CERN network

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.1-3
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 1.1-2
- fix requires on puppet-agent

* Tue Feb 16 2021 Ben Morrice <ben.morrice@cern.ch> - 1.1-1
- move away from hiera lookups

* Mon Jan 27 2020 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
-Initial release

